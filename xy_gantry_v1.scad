include <../../config/constants.scad>
include <NopSCADlib/core.scad>
include <NopSCADlib/vitamins/rails.scad>
include <NopSCADlib/vitamins/stepper_motor.scad>
include <NopSCADlib/vitamins/stepper_motors.scad>
include <NopSCADlib/vitamins/pulleys.scad>
include <NopSCADlib/vitamins/pulley.scad>
include <NopSCADlib/vitamins/belts.scad>
include <NopSCADlib/vitamins/belt.scad>
include <NopSCADlib/vitamins/nut.scad>
include <NopSCADlib/vitamins/nuts.scad>
include <NopSCADlib/vitamins/screw.scad>
include <NopSCADlib/vitamins/screws.scad>
include <../../gantry/x_carriage/toolchange_carriage/toolchanger_mount.scad>
include <xy_bracket.scad>

use <NopSCADlib/vitamins/insert.scad>
use <NopSCADlib/utils/layout.scad>
use <NopSCADlib/utils/layout.scad>
use <NopSCADlib/vitamins/nut.scad>
use <../profiles/aluProfile_23_5.scad>
use <../profiles/aluProfile_15_0.scad>
use <../tools/screw_generator.scad>

// visualisation only
showExtrusion=1;
showXRail=1;
showYRail=1;
show_xy_bracket=1;

// stepper motor conf
$extrusion_width = 0.5;
$pp1_colour = "dimgrey";
$pp2_colour = [0.9, 0.9, 0.9];
//echo(extrusion_width = extrusion_width, layer_height = layer_height);
motor_num=4;
m=NEMA17;
$i=motor_num;
// end stepper motor conf
// belt type
belt = GT2x6;
// shift the height of belts,pulleys and motors
shiftBeltHight = 4;
// position of rail carriages
// 0=center, 1=front, -1=back
pos = 0; //[-1 : 0.1 : 1]

//=== STEPPER MOTOR BRACKET ===
stepperMotorBracket();
module stepperMotorBracket(){
    
    //block cutoff height - cylinders
    bch=13;
    //translate([238.5,204,-13.875])
    //cube([70,92,4.2],center=true);  
    difference(){
    translate([(238.5-(pkl/2)),200,(-13.875+(pkl/2))])
    // pkl from constants.scad
    cube([70-pkl,100,4.2+pkl],center=true); 
        //stepper motor hole
        translate([226,179,-0]) 
    cylinder(h=bch+40,d=30);
        
 
 hull(){//hull 1
   translate([226,240,-13]) 
    cylinder(h=bch,d=20);
   translate([226,179,-13]) 
    cylinder(h=bch,d=20);
  }//end hull 1
  hull(){//hull 2
  translate([213.5,228,-13]) 
  cylinder(h=bch,d=18);  
  translate([190,228,-13]) 
  cylinder(h=bch,d=18);
  }// end hull 2
  hull(){//hull 3
      translate([226,240,-13]) 
    cylinder(h=bch,d=20);
      translate([190,240,-13]) 
    cylinder(h=bch,d=20);
  }//end hull3
  
  hull(){//hull 4
       translate([226,179,-13]) 
    cylinder(h=bch,d=20);
       translate([200,179,-13]) 
    cylinder(h=bch,d=20);
  }//end hull 4
  translate([(238.5-(pkl/2))-6,202.5+5.1/2,(-13.875+(pkl/2))+5.1/2-15])
    // pkl from constants.scad
    cube([64-pkl-5,100-10,4.2+pkl-30],center=true); 
        
    // stepper motor screw holes
        //#cylinder(h=30,d=3.3);
        translate([210.5,194.5,-14])
    M3screwGenerator(10,26,0,0,false);
        translate([241.5,194.5,-14])
    M3screwGenerator(10,26,0,0,false);
  translate([241.5,194.5-31,-14])
    M3screwGenerator(10,26,0,0,false);
  translate([241.5-31,194.5-31,-14])
    M3screwGenerator(10,26,0,0,false);
  
      translate([226,240,4]) 
    M3screwGenerator(20,8,0,0,false);
    translate([213.5,228,4]) 
    M3screwGenerator(20,8,0,0,false); 

    // bracket to alu extrusion screw holes
    
       
    
    }
}

//=== STEPPER MOTOR BRACKET END ===

//=== BELTS ===
module belt_test() {
    x_shift=-18;
    pth_val = 240;
    p2 = [-pth_val, -pth_val+14];
    p4 = [-pth_val+12, -pth_val+14+ pulley_pr(GT2x20_toothed_idler)*2 ];
    p3 = [-pth_val+61, -pth_val+14];
    p5 = [-pth_val, pth_val];
    p6 = [pth_val,  pth_val];

    p7 = [pth_val  + pulley_pr(GT2x20ob_pulley) - pulley_pr(GT2x16_plain_idler), +pulley_pr(GT2x16_plain_idler)+x_shift];
    p8 = [-pth_val + pulley_pr(GT2x20ob_pulley) + pulley_pr(GT2x16_plain_idler), -pulley_pr(GT2x16_plain_idler)+x_shift];

    module pulleys(flip = false) {
        translate(p4) rotate([0, flip ? 180 : 0, 0]) pulley_assembly(GT2x16_plain_idler);
        translate(p3) pulley_assembly(GT2x20ob_pulley);
        translate(p2) pulley_assembly(GT2x20_toothed_idler);
        translate(p5) pulley_assembly(GT2x20_toothed_idler);
        translate(p6) pulley_assembly(GT2x20_toothed_idler);
        translate(p7) {
            pulley = GT2x16_toothed_idler;
            screw = find_screw(hs_cs_cap, pulley_bore(pulley));
            insert = screw_insert(screw);
            hflip(flip) {
                pulley_assembly(pulley);
                //translate_z(pulley_height(pulley) + pulley_offset(pulley) + screw_head_depth(screw, pulley_bore(pulley)))
                  //  screw(screw, 25);

                //translate_z(pulley_offset(pulley) - insert_length(insert))
                  //  vflip()
                    //   insert(insert);
            }
        }
     
        translate(p8) pulley_assembly(GT2x16_plain_idler);
    }

    path = [ [p7.x, p7.y, pulley_pr(GT2x16_plain_idler)],
             [p8.x, p8.y, -pulley_pr(GT2x16_plain_idler)],
             [p4.x, p4.y, -pulley_pr(GT2x16_plain_idler)],
             [p3.x, p3.y, pulley_pr(GT2x20ob_pulley)],
             [p2.x, p2.y, pulley_pr(GT2x20ob_pulley)],
             [p5.x, p5.y, pulley_pr(GT2x20ob_pulley)],
             [p6.x, p6.y, pulley_pr(GT2x20ob_pulley)]
           ];
    
    
    translate_z(-1.5+shiftBeltHight){
        belt(belt, path, 35, [0,  0]);
        pulleys();
    }
    translate_z(-10.5+shiftBeltHight)
        hflip() {
            belt(belt, path, 35, [0,  0], belt_colour = grey(90), tooth_colour = grey(50));
            pulleys(flip=true);
    }      
}
//===BELTS END ===

//=== PULLEYS ===
module GT2x16_pulley(){
    rotate(-145)
        pulley_assembly(GT2x16_pulley);
}
module GT2x16_toothed_idler(){
    rotate(-145)
        pulley_assembly(GT2x16_toothed_idler);
}
//=== PULLEYS END ===

//=== RAILS ===
module MGN12H(){
    sheet = 23.5;
    l = MGN12H_carriage;
    carriage = MGN12H_carriage;
    rail = carriage_rail(carriage);
    length = 450;
    screw = rail_screw(rail);
    nut = screw_nut(screw);
    washer = screw_washer(screw);

    rail_assembly(carriage, length, pos * carriage_travel(carriage, length) / 2, 3<2 ? grey(20) : "green", 3<2 ? grey(20) : "red");
    /*
    rail_screws(rail, length, sheet + nut_thickness(nut, true) + washer_thickness(washer));

    rail_hole_positions(rail, length, 0)
        translate_z(-sheet)
            vflip()
                nut_and_washer(nut, true);
    */
}
module MGN12C(){
    sheet = 15;
    l = MGN12C_carriage;
    carriage = MGN12C_carriage;
    rail = carriage_rail(carriage);
    length = 400;
    screw = rail_screw(rail);
    nut = screw_nut(screw);
    washer = screw_washer(screw);

    rail_assembly(carriage, length, pos * 
    carriage_travel(carriage, length) / 2, 3<2 ? grey(20) : "green", 3<2 ? grey(20) : "red");
/*
    rail_screws(rail, length, sheet + nut_thickness(nut, true) + washer_thickness(washer));

    rail_hole_positions(rail, length, 0)
        translate_z(-sheet)
            vflip()
                nut_and_washer(nut, true);
    */
}
//=== RAILS END ===

//=== RAIL MOUNTED ON EXTRUSION ===
module rail_extrusion_23_5(){
     // mount the rail to the extrusion shifted in y position to align
     // carriage with outer profile. Screwing a housing plate 
     // is easier and does not block the carriage.
     shift_y_rail_pos=-2;
        if(showExtrusion){ 
            aluProfile_23_5(500);
        }
        if(showYRail){
            translate([0,shift_y_rail_pos,-11.8]) rotate([180,0,0])
            MGN12H();
        }
}
module rail_extrusion_15_0(){

   // translate([0,0,-28]){
     //   rotate([0,0,90])
       // aluProfile_15_0(630);
        if(showXRail){
            translate([0,0,-7.5]) rotate([180,0,90])
            MGN12C();
        }
   //}
}
//=== RAIL MOUNTED ON EXTRUSION END ===

//=== STEPPER MOTORS ===
module stepper_motors(){
shift_motor_y=50;
wall_thickness = 4;
translate([230-wall_thickness,-229+shift_motor_y,-20+shiftBeltHight])rotate([0,0,0]){
   NEMA(m, 0, m == NEMA17);
   //translate_z(4)
   //  NEMA_screws(m, M3_pan_screw, n = $i, earth = $i > 4 ? undef : $i - 1);

}    
translate([230-wall_thickness,229-shift_motor_y,-20+shiftBeltHight])rotate([0,0,180]){
   NEMA(m, 0, m == NEMA17);
   //translate_z(4)
   //  NEMA_screws(m, M3_pan_screw, n = $i, earth = $i > 4 ? undef : $i - 1);
}
}
//=== STEPPER MOTORS END ===

//=== XY BRACKET ===
if(show_xy_bracket){
xy_bracket_body_top();
xy_bracket_body_bottom();
xy_bracket_top_plate();
mirror([0,1,0]){
   xy_bracket_body_top();
   xy_bracket_body_bottom();
   xy_bracket_top_plate(); 
}
}
//=== XY BRACKET END ===


xy_gantry();
module xy_gantry(){
    if($preview){
    translate([0,250+(pkl/2),0])
    rail_extrusion_23_5();
    translate([0,-250-(pkl/2),0])mirror([0,1,0])
    rail_extrusion_23_5();
    translate([0,0,-9.3])
    rail_extrusion_15_0();
    //difference(){
    stepperMotorBracket();    
    stepper_motors();
    
    //}
    // alu extrusion
        if(showExtrusion){ 
            rotate([0,0,90])translate([0,250+(pkl/2),0])
            aluProfile_23_5(500);
            rotate([0,0,90])translate([0,-250-(pkl/2),0])
            aluProfile_23_5(500);
        }
        rotate([0,0,90])
        belt_test();
        //toolchanger_mount();
       
    }
}




